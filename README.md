## OpenAI Assistant with Access to Tools

> _We've been merging with tools since the beginning of human evolution, and arguably, that's one of the things that makes us human beings_.
      -- Franklin Foer

**Open AI Assistants**
- Multimodal
- Persistent
- File search
    - Upload upto 10,000 documents
    - Embedding
    - Vector store
    - Page rank
- Python interpreter
- Tools
    - Access external APIs, databases, graphQL
    - Up to 1000 tools
    - Able to use mulitple tools to accomplish tasks


Importantly, there is no conditional logic in the code, the LLM chooses when and how to use the tools.

As a proof of principle -- a blue devil bot:

**Blue Devil Bot**
- File Search
    - Faculty handbook
    - DGS manual
    - DGSA manual
- Python interpreter
- Tools
    - Duke Directory
    - Scholars 
    - Additional tech demonstrations
        - Chinook Music Database (SQL queries)
        - Star Wars SWAPI (GraphQL queries) 

### Brief intro to tools

A tool is just a json schema describing the parameters you want the model to
collect.

For example, To access a weather API you might want to collect city, state and temperature
unit.

``` json
{
    "Function Name": "Get Weather",
    "type": "object",
    "properties": {
        "city": {
            "type": "string",
            "description": "The city for which the weather information is requested."
        },
        "state": {
            "type": "string",
            "description": "The state where the city is located."
        },
        "temperature_unit": {
            "type": "string",
            "description": "The unit for temperature readings. Can be 'Fahrenheit', 'Celsius', or 'Kelvin'.",
            "enum": [
                "Fahrenheit",
                "Celsius",
                "Kelvin"
            ]
        }
    },
    "required": [
        "city",
        "state"
    ],
    "additionalProperties": false
}
```

So if a user queries the LLM assistant "Do I need a jacket today?", The LLM
assistant will realize they are probably asking about the weather, will check
the weather tool and realize that it doesn't have enough information to
populate the json parameters.  So the assistant will prompt the user for more
information  "where are you located?" in order to populate the parameters.  Once
populated, the LLM returns the parameters to the local server which then queries
the weather API with the parameters.  The result from the weather API is
returned back to the LLM assistant which then proceeds to convert it into a
conversational response.
